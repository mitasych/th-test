<?php
namespace app\rbac;

use yii\rbac\Rule;

class OwnerRule extends Rule
{
    public $name = 'isOwner';

    /**
     * {@inheritDoc}
     * @see \yii\rbac\Rule::execute()
     */
    public function execute($user, $item, $params)
    {
        return isset($params['account']) ? $params['account']->user_id == $user : false;
    }
}