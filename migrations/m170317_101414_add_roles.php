<?php

use yii\db\Migration;
use app\models\User;

class m170317_101414_add_roles extends Migration
{
    public $authManager = 'authManager';
    
    public function init()
    {
        parent::init();
        $this->authManager = \Yii::$app->get('authManager');
    }
    
    public function up()
    {
        $this->authManager->removeAll();
    
        $user = $this->authManager->createRole(User::ROLE_USER);
        $this->authManager->add($user);
    
        $admin = $this->authManager->createRole(User::ROLE_ADMIN);
        $this->authManager->add($admin);
    }
    
    public function down()
    {
        $this->authManager->remove($this->authManager->getRole(User::ROLE_ADMIN));
        $this->authManager->remove($this->authManager->getRole(User::ROLE_USER));
    }

}
