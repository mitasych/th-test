<?php

use yii\db\Migration;

/**
 * Handles the creation of table `transaction`.
 * Has foreign keys to the tables:
 *
 * - `account`
 * - `account`
 */
class m170317_114415_create_transaction_table extends Migration
{
    /**
     * @inheritdoc
     */
    public function up()
    {
        $this->createTable('transaction', [
            'id' => $this->primaryKey(),
            'sender_id' => $this->integer(),
            'receiver_id' => $this->integer()->notNull(),
            'type' => $this->integer(1)->notNull(),
            'amount' => $this->decimal(19,2)->notNull(),
            'created_at' => $this->integer(),
        ]);

        // creates index for column `sender_id`
        $this->createIndex(
            'idx-transaction-sender_id',
            'transaction',
            'sender_id'
        );

        // add foreign key for table `account`
        $this->addForeignKey(
            'fk-transaction-sender_id',
            'transaction',
            'sender_id',
            'account',
            'user_id',
            'CASCADE'
        );

        // creates index for column `receiver_id`
        $this->createIndex(
            'idx-transaction-receiver_id',
            'transaction',
            'receiver_id'
        );

        // add foreign key for table `account`
        $this->addForeignKey(
            'fk-transaction-receiver_id',
            'transaction',
            'receiver_id',
            'account',
            'user_id',
            'CASCADE'
        );
    }

    /**
     * @inheritdoc
     */
    public function down()
    {
        // drops foreign key for table `account`
        $this->dropForeignKey(
            'fk-transaction-sender_id',
            'transaction'
        );

        // drops index for column `sender_id`
        $this->dropIndex(
            'idx-transaction-sender_id',
            'transaction'
        );

        // drops foreign key for table `account`
        $this->dropForeignKey(
            'fk-transaction-receiver_id',
            'transaction'
        );

        // drops index for column `receiver_id`
        $this->dropIndex(
            'idx-transaction-receiver_id',
            'transaction'
        );

        $this->dropTable('transaction');
    }
}
