<?php

use yii\db\Migration;

/**
 * Handles the creation of table `account`.
 * Has foreign keys to the tables:
 *
 * - `user`
 */
class m170317_103538_create_account_table extends Migration
{
    /**
     * @inheritdoc
     */
    public function up()
    {
        $this->createTable('account', [
            'user_id' => $this->integer()->notNull(),
            'balance' => $this->decimal(19,2)->notNull()->defaultValue(0),
            'created_at' => $this->integer(),
            'updated_at' => $this->integer(),
            'PRIMARY KEY(user_id)',
        ]);

        // creates index for column `user_id`
        $this->createIndex(
            'idx-account-user_id',
            'account',
            'user_id'
        );

        // add foreign key for table `user`
        $this->addForeignKey(
            'fk-account-user_id',
            'account',
            'user_id',
            'user',
            'id',
            'CASCADE'
        );
    }

    /**
     * @inheritdoc
     */
    public function down()
    {
        // drops foreign key for table `user`
        $this->dropForeignKey(
            'fk-account-user_id',
            'account'
        );

        // drops index for column `user_id`
        $this->dropIndex(
            'idx-account-user_id',
            'account'
        );

        $this->dropTable('account');
    }
}
