<?php

use yii\db\Migration;
use app\models\User;
use app\models\UserForm;

class m170317_120352_add_admin_user extends Migration
{
    public $authManager = 'authManager';
    
    public function init()
    {
        parent::init();
        $this->authManager = \Yii::$app->get('authManager');
    }
    
    public function up()
    {
        $model = new UserForm();
    
        $model->username = 'admin';
    
        $model->save();
    
        $this->authManager->assign($this->authManager->getRole(User::ROLE_ADMIN), $model->user->id);
    }
    
    public function down()
    {
        $user = User::findByUsername('admin');
    
        $this->authManager->revoke($this->authManager->getRole(User::ROLE_ADMIN), $user->id);
    
        $user->delete();
    }
}
