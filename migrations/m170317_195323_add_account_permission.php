<?php

use yii\db\Migration;
use app\models\User;
use app\rbac\OwnerRule;

class m170317_195323_add_account_permission extends Migration
{
    public $authManager = 'authManager';
    public $adminRole;
    public $userRole;
    
    public function init()
    {
        parent::init();
        $this->authManager = \Yii::$app->authManager;
        
        $this->adminRole = $this->authManager->getRole(User::ROLE_ADMIN);
        $this->userRole = $this->authManager->getRole(User::ROLE_USER);
    }
    
    public function up()
    {
        $viewAccount = $this->authManager->createPermission('viewAccount');
        $viewAccount->description = 'View User Account';
        $this->authManager->add($viewAccount);
        
        $this->authManager->addChild($this->adminRole, $viewAccount);
        
        $rule = new OwnerRule();
        $this->authManager->add($rule);
        
        $viewOwnAccount = $this->authManager->createPermission('viewOwnAccount');
        $viewOwnAccount->description = 'View Own Account';
        $viewOwnAccount->ruleName = $rule->name;
        $this->authManager->add($viewOwnAccount);
        
        $this->authManager->addChild($viewOwnAccount, $viewAccount);
        $this->authManager->addChild($this->userRole, $viewOwnAccount);

    }

    public function down()
    {
        $this->authManager->remove($this->authManager->getPermission('viewOwnAccount'));
        
        $rule = new OwnerRule();
        
        $this->authManager->remove($this->authManager->getRule($rule->name));
        
        $this->authManager->remove($this->authManager->getPermission('viewAccount'));
    }
}
