<?php

namespace tests\models;

use app\models\LoginForm;
use Codeception\Specify;
use app\models\User;

class LoginFormTest extends \Codeception\Test\Unit
{
    private $model;

    protected function _after()
    {
        \Yii::$app->user->logout();
    }

    public function testLoginNoUser()
    {
        $this->model = new LoginForm([
            'username' => \Yii::$app->security->generateRandomString(10),
        ]);

        expect_that($this->model->login());
    }

    public function testExistingUser()
    {
        $this->model = new LoginForm([
            'username' => 'admin',
        ]);

        expect_that($this->model->login());
    }
    
    public function testLoginNoUserCheckBalance()
    {
        $this->model = new LoginForm([
            'username' => \Yii::$app->security->generateRandomString(10),
        ]);
    
        expect_that($this->model->login());
    
        $user = User::findIdentity(\Yii::$app->user->id);
    
        expect($user->account->balance)->equals(0.00);
    }

}
