<?php
namespace models;

use app\models\LoginForm;
use app\models\TransferForm;
use app\models\Transaction;
use app\models\User;
use app\models\Account;

class TransferTest extends \Codeception\Test\Unit
{

    private $loginModel;

    private $transferModel;

    /**
     *
     * @var \UnitTester
     */
    protected function _before()
    {}

    protected function _after()
    {
        \Yii::$app->user->logout();
    }
    
    // tests
    public function testTransferToExistingUser()
    {
        $this->loginModel = new LoginForm([
            'username' => \Yii::$app->security->generateRandomString(10)
        ]);
        
        $this->loginModel->login();
        
        $startMyBalance = Account::findOne([
            'user_id' => \Yii::$app->user->id
        ])->balance;
        
        $startReceiverBalance = User::findByUsername('admin')->account->balance;
        
        $amount = 28;
        
        $this->transferModel = new TransferForm([
            'receiver' => 'admin',
            'type' => Transaction::TYPE_PERSON_TRANSFER,
            'amount' => $amount
        ]);
        
        expect_that($this->transferModel->transfer());
        expect_not(! empty($this->model->errors));
        $this->assertTrue(User::findByUsername('admin')->account->balance == $startReceiverBalance + $amount && Account::findOne([
            'user_id' => \Yii::$app->user->id
        ])->balance == $startMyBalance - $amount);
    }

    public function testTransferToNotExistingUser()
    {
        $this->loginModel = new LoginForm([
            'username' => 'admin'
        ]);
        
        $this->loginModel->login();
        
        $startMyBalance = User::findByUsername('admin')->account->balance;
        
        $amount = 32;
        
        $receiver = \Yii::$app->security->generateRandomString(10);
        
        $this->transferModel = new TransferForm([
            'receiver' => $receiver,
            'type' => Transaction::TYPE_PERSON_TRANSFER,
            'amount' => $amount
        ]);
        
        expect_that($this->transferModel->transfer());
        expect_not(!empty($this->model->errors));
        
        $user = User::findByUsername($receiver);
        
        $this->assertTrue(User::findByUsername('admin')->account->balance == $startMyBalance - $amount && $user->account->balance == $amount);
    }
    
    public function testTransferToMyself()
    {
        $username = 'testMyself';
        
        $this->loginModel = new LoginForm([
            'username' => $username
        ]);
        
        $this->loginModel->login();
        
        $amount = 45;
        
        $this->transferModel = new TransferForm([
            'receiver' => $username,
            'type' => Transaction::TYPE_PERSON_TRANSFER,
            'amount' => $amount
        ]);
        
        expect_not($this->transferModel->transfer());
        expect($this->transferModel->errors)->hasKey('receiver');
    }
    
    public function testTransferNegativeAmount()
    {
        $this->loginModel = new LoginForm([
            'username' => 'admin'
        ]);
        
        $this->loginModel->login();
        
        $amount = -0.1;
        
        $receiver = \Yii::$app->security->generateRandomString(10);
        
        $this->transferModel = new TransferForm([
            'receiver' => $receiver,
            'type' => Transaction::TYPE_PERSON_TRANSFER,
            'amount' => $amount
        ]);
        
        expect_not($this->transferModel->transfer());
        expect($this->transferModel->errors)->hasKey('amount');
    }
    
    public function testTransferAndCheckNegativeBalance()
    {
        $username = \Yii::$app->security->generateRandomString(10);
        
        $this->loginModel = new LoginForm([
            'username' => $username
        ]);
        
        $this->loginModel->login();
        
        $user = User::findIdentity(\Yii::$app->user->id);
        
        expect($user->account->balance)->equals(0.00);
        
        $this->transferModel = new TransferForm([
            'receiver' => 'admin',
            'type' => Transaction::TYPE_PERSON_TRANSFER,
            'amount' => 120
        ]);
        
        expect_that($this->transferModel->transfer());
        expect(Account::findOne(['user_id'=>\Yii::$app->user->id])->balance)->equals(-120.00);
    }

    public function testRefillAccountByAdmin()
    {
        $this->loginModel = new LoginForm([
            'username' => 'admin'
        ]);
        
        $this->loginModel->login();
        
        $amount = 56;
        
        $receiver = \Yii::$app->security->generateRandomString(10);
        
        $this->transferModel = new TransferForm([
            'receiver' => $receiver,
            'type' => Transaction::TYPE_REFILL,
            'amount' => $amount
        ]);
        
        expect_that($this->transferModel->transfer());
        expect($this->transferModel->errors)->hasntKey('type');
        expect(Account::findOne([
            'user_id' => User::findByUsername($receiver)->id
        ])->balance)->equals($amount);
    }
    
    public function testRefillAccountByUser()
    {
        $this->loginModel = new LoginForm([
            'username' => 'userRefill'
        ]);
        
        $this->loginModel->login();
        
        $amount = 18;
        
        $receiver = \Yii::$app->security->generateRandomString(10);
        
        $this->transferModel = new TransferForm([
            'receiver' => $receiver,
            'type' => Transaction::TYPE_REFILL,
            'amount' => $amount
        ]);
        
        expect_not($this->transferModel->transfer());
        expect($this->transferModel->errors)->hasKey('type');
    }
}