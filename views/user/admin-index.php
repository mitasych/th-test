<?php

use yii\helpers\Html;
use yii\grid\GridView;
use yii\widgets\Pjax;
use yii\helpers\Url;
/* @var $this yii\web\View */
/* @var $searchModel app\models\search\UserSearch */
/* @var $dataProvider yii\data\ActiveDataProvider */

$this->title = Yii::t('app', 'Users');
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="user-index">

    <h1><?= Html::encode($this->title) ?></h1>
    <?php // echo $this->render('_search', ['model' => $searchModel]); ?>

    <p>
        <?= Html::a(Yii::t('app', 'Create User'), ['create'], ['class' => 'btn btn-success']) ?>
    </p>
<?php Pjax::begin(); ?>    <?= GridView::widget([
        'dataProvider' => $dataProvider,
        'filterModel' => $searchModel,
        'columns' => [
            ['class' => 'yii\grid\SerialColumn'],

            'id',
            'username',
            'created_at:datetime',
            [
                'header' => 'Balance',
                'value' => function($data) {
                    return $data->account->balance;
                },
            ],

            [
            'class' => 'yii\grid\ActionColumn',
            'template' => '{view} {update} {balance} {delete}',
                'buttons' => [
                    'balance' => function ($url, $model, $key) {
                    $url = Url::to([
                        '/user/account',
                        'id' => $model->account->user_id
                    ]);
                    $options = [
                        'title' => 'Account',
                        'aria-label' => 'Account',
                        'data-pjax' => '0'
                    ];
                    return Html::a('<span class="glyphicon glyphicon-usd"></span>', $url, $options);
                    }
                ]
            ],
        ],
    ]); ?>
<?php Pjax::end(); ?></div>
