<?php

use yii\helpers\Html;
use yii\widgets\DetailView;
use yii\grid\GridView;
use yii\base\Widget;
use yii\widgets\Pjax;
use app\models\Transaction;
use app\models\User;

/* @var $this yii\web\View */
/* @var $model app\models\User */

$this->title = $model->user->username;
$this->params['breadcrumbs'][] = ['label' => Yii::t('app', 'Users'), 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;

$dataIncoming->pagination->pageParam = 'incoming-page';
$dataIncoming->sort->sortParam = 'incoming-sort';

$dataOutgoing->pagination->pageParam = 'outgoing-page';
$dataOutgoing->sort->sortParam = 'outgoing-sort';
?>
<div class="user-view">

    <h1><?= Html::encode($this->title) ?></h1>

	<p>
        <?php echo Html::a(Yii::t('app', 'Transfer funds'), ['transfer'], ['class' => 'btn btn-primary']) ?>
        <?php if (Yii::$app->user->can(User::ROLE_ADMIN)):?>
	        <?php echo Html::a(Yii::t('app', 'Refill Account'), ['transfer', 'type'=>Transaction::TYPE_REFILL, 'name'=>$model->user->username], ['class' => 'btn btn-primary']) ?>
        <?php endif; ?>
        <?php // echo Html::a(Yii::t('app', 'Delete'), ['delete', 'id' => $model->id], [
//             'class' => 'btn btn-danger',
//             'data' => [
//                 'confirm' => Yii::t('app', 'Are you sure you want to delete this item?'),
//                 'method' => 'post',
//             ],
//         ]) ?>
    </p>

    <?= DetailView::widget([
        'model' => $model,
        'attributes' => [
            'balance',
            'created_at:datetime',
            'updated_at:datetime',
        ],
    ]) ?>
    
    <ul class="nav nav-tabs">
      <li class="active"><a data-toggle="tab" href="#incoming">Incoming Transactions</a></li>
      <li><a data-toggle="tab" href="#outgoing">Outgoing Transactions</a></li>
    </ul>
    
    <div class="tab-content">
      <div id="incoming" class="tab-pane fade in active">
      <?php Pjax::begin(['id'=>'incoming-grid-wrapper']);?>
        <?php echo GridView::widget([
            'id'=>'incoming-grid',
            'dataProvider' => $dataIncoming,
            'columns' => [
                ['class' => 'yii\grid\SerialColumn'],
                'id',
                [
                    'label' => 'Sender',
                    'value' => function($data) {
                        
                        return $data->type==Transaction::TYPE_REFILL ? Transaction::typeName($data->type) : $data->sender->user->username;
                    },
                ],
                'amount',
                'created_at:datetime',
            ],
        ]); ?>
        <?php Pjax::end(); ?>
      </div>
      <div id="outgoing" class="tab-pane fade">
      	<?php Pjax::begin(['id'=>'outgoing-grid-wrapper']);?>
        <?php echo GridView::widget([
            'id'=>'outgoing-grid',
            'dataProvider' => $dataOutgoing,
            'columns' => [
                ['class' => 'yii\grid\SerialColumn'],
                'id',
                [
                    'label' => 'Receiver',
                    'value' => function($data) {
                        return $data->receiver->user->username;
                    },
                ],
                'amount',
                'created_at:datetime',
            ],
        ]); ?>
        <?php Pjax::end(); ?>
      </div>
    </div>
    
</div>
