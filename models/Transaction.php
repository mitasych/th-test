<?php

namespace app\models;

use Yii;
use yii\behaviors\TimestampBehavior;

/**
 * This is the model class for table "{{%transaction}}".
 *
 * @property integer $id
 * @property integer $sender_id
 * @property integer $receiver_id
 * @property integer $type
 * @property string $amount
 * @property integer $created_at
 *
 * @property Account $receiver
 * @property Account $sender
 */
class Transaction extends \yii\db\ActiveRecord
{
    const TYPE_REFILL = 1;
    const TYPE_PERSON_TRANSFER = 2;

    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return '{{%transaction}}';
    }
    
    /**
     * @inheritdoc
     */
    public function behaviors()
    {
        return [
            [
                'class'=>TimestampBehavior::className(),
                'updatedAtAttribute'=>false,
            ]
    
        ];
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['sender_id', 'receiver_id', 'type', 'created_at'], 'integer'],
            [['receiver_id', 'type', 'amount'], 'required'],
            [['amount'], 'number'],
            [['receiver_id'], 'exist', 'skipOnError' => true, 'targetClass' => Account::className(), 'targetAttribute' => ['receiver_id' => 'user_id']],
            [['sender_id'], 'exist', 'skipOnError' => true, 'targetClass' => Account::className(), 'targetAttribute' => ['sender_id' => 'user_id']],
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id' => Yii::t('app', 'ID'),
            'sender_id' => Yii::t('app', 'Sender ID'),
            'receiver_id' => Yii::t('app', 'Receiver ID'),
            'type' => Yii::t('app', 'Type'),
            'amount' => Yii::t('app', 'Amount'),
            'created_at' => Yii::t('app', 'Created At'),
        ];
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getReceiver()
    {
        return $this->hasOne(Account::className(), ['user_id' => 'receiver_id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getSender()
    {
        return $this->hasOne(Account::className(), ['user_id' => 'sender_id']);
    }
    
    /**
     *
     * {@inheritDoc}
     * @see \yii\db\BaseActiveRecord::afterSave()
     */
    public function afterSave($insert, $changedAttributes)
    {
        parent::afterSave($insert, $changedAttributes);
    
        if ($insert) {
            $this->receiver->balance += $this->amount;
            $this->receiver->update(true, ['balance']);
            if (!is_null($this->sender)) {
                $this->sender->balance -= $this->amount;
                $this->sender->update(true, ['balance']);
            }
        }
    }
    
    public static function typeName($type)
    {
        $names = [
            self::TYPE_REFILL=>'Refill by admin',
            self::TYPE_PERSON_TRANSFER=>'Person-to-person transaction'
        ];
        
        return $names[$type];
    }
}
