<?php

namespace app\models;

use Yii;
use yii\base\Model;

class TransferForm extends Model
{
    public $amount;
    public $receiver;
    public $type;


    public function rules()
    {
        return [
            [['receiver', 'amount', 'type'], 'required'],
            ['type', 'integer'],
            ['type', 'validateUserRole'],
            ['amount', 'number'],
            [['amount'], 'compare', 'compareValue' => 0, 'operator' => '>'],
            ['receiver', 'string'],
            [['receiver'], 'validateReceiver']
        ];
    }
    
    public function validateUserRole()
    {
            if (!\Yii::$app->user->can(User::ROLE_ADMIN) && $this->type == Transaction::TYPE_REFILL) {
                $this->addError('type', Yii::t('app', 'You can not use this type.'));
            }
    }
    
    public function validateReceiver()
    {
        if($this->type == Transaction::TYPE_PERSON_TRANSFER && $this->receiver == User::findOne(\Yii::$app->user->id)->username){
            $this->addError('receiver', Yii::t('app', 'You can not transfer funds to yourself.'));
        }
    }

    /**
     * Logs in a user using the provided username and password.
     * @return bool whether the user is logged in successfully
     */
    public function transfer()
    {
        if ($this->validate() && $this->getReceiverAccount()) {
            $account = Account::findOne(['user_id'=>\Yii::$app->user->id]);
            
            $transaction = new Transaction();
            $transaction->amount = $this->amount;
            $transaction->type = $this->type;
            if ($this->type == Transaction::TYPE_PERSON_TRANSFER) {
                $transaction->sender_id = $account->user_id;
            }
            $transaction->receiver_id = $this->receiverAccount->user_id;
            if (!$transaction->save()) {
                throw new Exception("The transaction can not be completed");
            }
            
            return true;
        }
        return false;
    }

    /**
     * Finds user by [[username]]
     *
     * @return User|null
     */
    public function getReceiverAccount()
    {
        $receiver = User::findByUsername($this->receiver);
        
        if (is_null($receiver)) {
            $model = new UserForm(['username'=>$this->receiver]);
            if (!$model->save()) {
                throw new Exception("Receiver couldn't be saved");
            }
            $receiver = $model->user;
        }
        
        return $receiver->account;
    }
}
