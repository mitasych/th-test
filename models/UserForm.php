<?php

namespace app\models;

use Yii;
use yii\base\Model;
use yii\db\Exception;

/**
 * LoginForm is the model behind the login form.
 *
 * @property User|null $user This property is read-only.
 *
 */
class UserForm extends Model
{
    public $username;

    private $_user = false;


    /**
     * @return array the validation rules.
     */
    public function rules()
    {
        return [
            // username and password are both required
            [['username'], 'required'],
        ];
    }


    /**
     * Signs user up.
     *
     * @return User|null the saved model or null if saving fails
     */
    public function save()
    {
        if ($this->validate()) {
            $user = $this->getUser();
            $user->username = $this->username;
            
            if(!$user->save()) {
                throw new Exception("User couldn't be  saved");
            };
            
            return !$user->hasErrors();
        }
        return null;
    }
    
    /**
     * @param User $model
     * @return mixed
     */
    public function setUser($model)
    {
        $this->username = $model->username;
        $this->_user = $model;
        
        return $this->_user;
    }

    /**
     * Finds user by [[username]]
     *
     * @return User|null
     */
    public function getUser()
    {
        if (!$this->_user) {
            $this->_user = new User();
        }

        return $this->_user;
    }
}
