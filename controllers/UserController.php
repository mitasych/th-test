<?php

namespace app\controllers;

use Yii;
use yii\filters\AccessControl;
use app\models\User;
use app\models\search\UserSearch;
use yii\web\Controller;
use yii\web\NotFoundHttpException;
use yii\filters\VerbFilter;
use app\models\Account;
use app\models\Transaction;
use yii\data\ActiveDataProvider;
use app\models\TransferForm;
use yii\base\View;
use yii\web\ForbiddenHttpException;

/**
 * UserController implements the CRUD actions for User model.
 */
class UserController extends Controller
{
    /**
     * @inheritdoc
     */
    public function behaviors()
    {
        return [
            'access' => [
                'class' => AccessControl::className(),
                'rules' => [
                    [
                        'actions' => ['index'],
                        'allow' => true,
                        'roles' => ['?','@'],
                    ],
                    [
                        'actions' => ['account','transfer'],
                        'allow' => true,
                        'roles' => ['@'],
                    ],
                    [
                        'actions' => ['admin','create','update','delete', 'view'],
                        'allow' => true,
                        'roles' => ['admin'],
                    ],
                ],
            ],
            'verbs' => [
                'class' => VerbFilter::className(),
                'actions' => [
                    'delete' => ['POST'],
                ],
            ],
        ];
    }

    /**
     * Lists all User models.
     * @return mixed
     */
    public function actionAdmin()
    {
        $searchModel = new UserSearch();
        $dataProvider = $searchModel->search(Yii::$app->request->queryParams);

        return $this->render('admin-index', [
            'searchModel' => $searchModel,
            'dataProvider' => $dataProvider,
        ]);
    }
    
    /**
     * public listing User models .
     * @return mixed
     */
    public function actionIndex()
    {
        $searchModel = new UserSearch();
        $dataProvider = $searchModel->search(Yii::$app->request->queryParams);

        return $this->render('index', [
            'searchModel' => $searchModel,
            'dataProvider' => $dataProvider,
        ]);
    }

    /**
     * Displays a single User model.
     * @param integer $id
     * @return mixed
     */
    public function actionView($id)
    {
        return $this->render('view', [
            'model' => $this->findModel($id),
        ]);
    }
    
    /**
     * Displays own User account.
     * @param integer $id
     * @return mixed
     */
    public function actionAccount($id)
    {
        $model = Account::findOne($id);
        
        if (\Yii::$app->user->can('viewAccount', ['account'=>$model])) {
            $queryOutgoing = Transaction::find()->where(['sender_id'=>$model->user_id]);
            $dataOutgoing = new ActiveDataProvider([
                'query' => $queryOutgoing,
            ]);
            
            $queryIncoming = Transaction::find()->where(['receiver_id'=>$model->user_id]);
            $dataIncoming = new ActiveDataProvider([
                'query' => $queryIncoming,
            ]);
            
            
            return $this->render('account', [
                'model' => $model,
                'dataOutgoing'=>$dataOutgoing,
                'dataIncoming'=>$dataIncoming,
            ]);
        }
        else {
            throw new ForbiddenHttpException(Yii::t('yii', 'You are not allowed to perform this action.'));
        }
    }
    
    /**
     * Transfers funds to user
     * @param integer $type
     * @param string $name
     * @return mixed
     */
    public function actionTransfer($type=Transaction::TYPE_PERSON_TRANSFER, $name = NULL)
    {
        $model = new TransferForm();
        $model->type = $type;
        
        if ($type == Transaction::TYPE_REFILL) {
            $model->receiver = $name;
        }
        
        if ($model->load(Yii::$app->request->post()) && $model->transfer()) {
            
            $id = $model->type == Transaction::TYPE_REFILL ? $model->receiverAccount->user_id : \Yii::$app->user->id;
            
            return $this->redirect(['account', 'id'=>$id]);
        } else {
            return $this->render('transfer', [
                'model' => $model,
            ]);
        }
    }

    /**
     * Creates a new User model.
     * If creation is successful, the browser will be redirected to the 'view' page.
     * @return mixed
     */
    public function actionCreate()
    {
        $model = new User();

        if ($model->load(Yii::$app->request->post()) && $model->save()) {
            return $this->redirect(['view', 'id' => $model->id]);
        } else {
            return $this->render('create', [
                'model' => $model,
            ]);
        }
    }

    /**
     * Updates an existing User model.
     * If update is successful, the browser will be redirected to the 'view' page.
     * @param integer $id
     * @return mixed
     */
    public function actionUpdate($id)
    {
        $model = $this->findModel($id);

        if ($model->load(Yii::$app->request->post()) && $model->save()) {
            return $this->redirect(['view', 'id' => $model->id]);
        } else {
            return $this->render('update', [
                'model' => $model,
            ]);
        }
    }

    /**
     * Deletes an existing User model.
     * If deletion is successful, the browser will be redirected to the 'index' page.
     * @param integer $id
     * @return mixed
     */
    public function actionDelete($id)
    {
        $this->findModel($id)->delete();

        return $this->redirect(['index']);
    }

    /**
     * Finds the User model based on its primary key value.
     * If the model is not found, a 404 HTTP exception will be thrown.
     * @param integer $id
     * @return User the loaded model
     * @throws NotFoundHttpException if the model cannot be found
     */
    protected function findModel($id)
    {
        if (($model = User::findOne($id)) !== null) {
            return $model;
        } else {
            throw new NotFoundHttpException('The requested page does not exist.');
        }
    }
}
